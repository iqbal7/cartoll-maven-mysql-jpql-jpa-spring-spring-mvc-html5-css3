package se.par.amsen.cartoll.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.domain.TaxLevel;
import se.par.amsen.cartoll.domain.vehicle.Car;
import se.par.amsen.cartoll.repository.TaxIntervalRepository;
import se.par.amsen.cartoll.service.impl.TaxServiceImpl;

public class PriceServiceImplTest {

	private TaxServiceImpl taxService = new TaxServiceImpl();
	private TaxIntervalRepository repository;
	private Passage passage1;
	private Passage passage2;
	private Car car;
	
	private TaxInterval interval1 = new TaxInterval(9,0,9,59,TaxLevel.LOW);
	private TaxInterval interval2 = new TaxInterval(10,0,10,59,TaxLevel.MEDIUM);
	private TaxInterval interval3 = new TaxInterval(11,0,16,59,TaxLevel.HIGH);
	
	public List<TaxInterval> taxIntervals;
	
	@Before
	public void setup() {
		repository = createMock(TaxIntervalRepository.class);
		
		//init entities
		car = new Car("ABC-123", null);
		//passage set at HOUR:09 MINUTE:32 - should be price lvl 0
		passage1 = new Passage(car, 9, 32, null);
		//passage set at HOUR:16 MINUTE:03 - should be price lvl 3
		passage2 = new Passage(car, 16, 3, null);
		
		//init & fill list with mock price intervals
		taxIntervals = new ArrayList<TaxInterval>();
		taxIntervals.add(interval1);
		taxIntervals.add(interval2);
		taxIntervals.add(interval3);
	}
	
	@Test
	public void testCalculateTax() {
		//Setup repo method
		expect(repository.getTaxIntervals()).andReturn(taxIntervals).times(3);
		replay(repository);
		
		//Add repo mock to priceService
		taxService.setPriceIntervalRepository(repository);
		
		assertEquals("Tax calculation 1", car.getTaxByLevel(TaxLevel.LOW), taxService.calculateTax(passage1));
		assertEquals("Tax calculation 2", car.getTaxByLevel(TaxLevel.HIGH), taxService.calculateTax(passage2));
		assertNotEquals("Tax calculation (false test)", car.getTaxByLevel(TaxLevel.NO_TAX), taxService.calculateTax(passage2));
		verify(repository);
	}
}