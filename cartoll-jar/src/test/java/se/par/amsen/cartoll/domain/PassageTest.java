package se.par.amsen.cartoll.domain;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

import se.par.amsen.cartoll.domain.vehicle.Car;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;

public class PassageTest {

	private final static int HOUR = 3;
	private final static int MINUTE = 35;
	private final static Vehicle VEHICLE = new Car("ABC-123", null);
	private final static TollStation TOLL_STATION = new TollStation(null, null);

	private DateTime date;
	
	private Passage passage; 
	
	@Before
	public void setup() {
		passage = new Passage(VEHICLE, HOUR, MINUTE, TOLL_STATION);
		
		LocalTime localTime = new LocalTime(HOUR,MINUTE);
		date = localTime.toDateTimeToday();
	}
	
	@Test
	public void testConstructor() {
		assertEquals("DateTime", date, passage.getDate());
		assertEquals("Vehicle", VEHICLE, passage.getVehicle());
		assertEquals("ID", TOLL_STATION, passage.getTollStation());
	}
	
	@Test
	public void testSetId(){
		long id = 1;
		passage.setId(id);
		
		assertEquals("setId", id, passage.getId());
	}
}
