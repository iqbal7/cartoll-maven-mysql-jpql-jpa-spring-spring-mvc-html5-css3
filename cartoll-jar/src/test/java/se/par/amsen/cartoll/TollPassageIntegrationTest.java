package se.par.amsen.cartoll;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.domain.vehicle.Car;
import se.par.amsen.cartoll.domain.vehicle.Taxi;
import se.par.amsen.cartoll.domain.vehicle.Truck;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.service.AdressService;
import se.par.amsen.cartoll.service.OwnerService;
import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.TollStationService;
import se.par.amsen.cartoll.service.VehicleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application_context_mock_jpa.xml"})

public class TollPassageIntegrationTest {
	
	private Adress ownerAdress;
	private Adress stationAdress1;
	private Adress stationAdress2;

	private Owner owner;
	
	private TollStation tollStation1;
	private TollStation tollStation2;
	
	private Vehicle car;
	private Vehicle taxiEco;
	private Vehicle taxiNonEco;
	private Vehicle truckLight;
	private Vehicle truckHeavy;
	
	private Passage passage1;
	private Passage passage2;
	private Passage passage3;
	private Passage passage4;
	private Passage passage5;

	@Autowired
	private AdressService adressService;
	@Autowired
	private OwnerService ownerService;
	@Autowired
	private PassageService passageService;
	@Autowired
	private TollStationService tollStationService;
	@Autowired
	private VehicleService vehicleService;
	
	@Before 
	public void setup() {
		//TODO clear all repos first!!
		adressService.createAdress(new Adress("Möllstorpsgatan 12", "Färjestaden", "38632"));
		adressService.createAdress(new Adress("Fabriksgatan 2", "Göteborg", "41257"));
		adressService.createAdress(new Adress("Karlfeldtsgatan 16", "Göteborg", "41268"));
		
		owner = ownerService.getOwnerById(
				ownerService.createOwner(new Owner("Nils", "Amsen", "19920818XXXX", ownerAdress,null)));
		
		tollStation1 = tollStationService.getTollStationById(
				tollStationService.createTollStation(
						new TollStation("Fabriksgatan", stationAdress1)));
		
		tollStation2 = tollStationService.getTollStationById(
				tollStationService.createTollStation(
						new TollStation("Karlfeldtsgatan", stationAdress2)));
		
		car = vehicleService.getVehicleById(vehicleService.createVehicle(new Car("ABC-123", owner)));
		taxiEco = vehicleService.getVehicleById(vehicleService.createVehicle(new Taxi("BCE-234", owner, true)));
		taxiNonEco = vehicleService.getVehicleById(vehicleService.createVehicle(new Taxi("CED-345", owner, false)));
		truckLight = vehicleService.getVehicleById(vehicleService.createVehicle(new Truck("EDF-456", owner, 5000)));
		truckHeavy = vehicleService.getVehicleById(vehicleService.createVehicle(new Truck("DFG-567", owner, 8500)));
		
		passage1 = passageService.getPassageByPassageId(
				passageService.createPassageAndCalculateTax(new Passage(car, 7, 0, tollStationService.getTollStationById(tollStation1.getId()))));
		passage2 = passageService.getPassageByPassageId(
				passageService.createPassageAndCalculateTax(new Passage(taxiNonEco, 9, 0, tollStationService.getTollStationById(tollStation1.getId()))));
		passage3 = passageService.getPassageByPassageId(
				passageService.createPassageAndCalculateTax(new Passage(taxiEco, 11, 0, tollStationService.getTollStationById(tollStation1.getId()))));
		passage4 = passageService.getPassageByPassageId(
				passageService.createPassageAndCalculateTax(new Passage(truckLight, 13, 0, tollStationService.getTollStationById(tollStation2.getId()))));
		passage5 = passageService.getPassageByPassageId(
				passageService.createPassageAndCalculateTax(new Passage(truckHeavy, 15, 0, tollStationService.getTollStationById(tollStation2.getId()))));
	}
	
	@Test
	public void entitiesAndServiceIntegrationTest() {
		assertEquals("Num of owners", 1, ownerService.getOwners().size());
		assertEquals("Num of tollstations", 2, tollStationService.getTollStations().size());
		assertEquals("Num of passages", 5, passageService.getPassages().size());
		
		assertEquals("Validate passage 1", 18, passageService.getPassageByPassageId(passage1.getId()).getTax());
		assertEquals("Validate passage 2", 15, passageService.getPassageByPassageId(passage2.getId()).getTax());
		assertEquals("Validate passage 3", 10, passageService.getPassageByPassageId(passage3.getId()).getTax());
		assertEquals("Validate passage 4", 12, passageService.getPassageByPassageId(passage4.getId()).getTax());
		assertEquals("Validate passage 5", 40, passageService.getPassageByPassageId(passage5.getId()).getTax());
	}
}