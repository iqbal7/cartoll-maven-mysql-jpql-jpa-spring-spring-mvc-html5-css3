package se.par.amsen.cartoll.repository.jpa;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.domain.vehicle.Car;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.AdressRepository;
import se.par.amsen.cartoll.repository.TollStationRepository;
import se.par.amsen.cartoll.repository.VehicleRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application_context_mock_jpa.xml"})

public class TollStationRepositoryJpaTest {

	@Autowired
	TollStationRepository repo;
	
	@Autowired
	AdressRepository adressRepo;
	
	TollStation tollStation;
	Adress adress;
	
	@Before
	public void setUp() throws Exception {
		repo.clearAllTollStations();
		adressRepo.clearAllAdresses();
		adress = adressRepo.getAdressById(adressRepo.createAdress(new Adress("Högerkroken", "111111", "Way Up")));
		tollStation = new TollStation("Fool Station", adress);
	}
	
	@After
	public void tearDown() {
		repo.clearAllTollStations();
		adressRepo.clearAllAdresses();
	}

	@Test
	public void testCreateTollStation() {
		assertEquals("Test TollStation id", true, repo.createTollStation(tollStation) > 0);
	}

	@Test
	public void testUpdateTollStation() {
		repo.createTollStation(tollStation);
		tollStation.setName("Cool Station");
		repo.updateTollStation(tollStation);
		
		assertEquals("TollStation is updated", "Cool Station", repo.getTollStationById(tollStation.getId()).getName());
	}

	@Test
	public void testGetTollStations() {
		repo.createTollStation(new TollStation("Foolio Station", adress));
		repo.createTollStation(new TollStation("Coolio Station", adress));
		repo.createTollStation(new TollStation("Bro-lio Station", adress));
		
		assertEquals("No. of TollStations are 3", 3, repo.getTollStations().size());
	}

	@Test
	public void testGetTollStationById() {
		repo.createTollStation(tollStation);
		assertEquals("Get TollStation", tollStation.getId(), repo.getTollStationById(tollStation.getId()).getId());
	}

	@Test
	public void testRemoveTollStation() {
		repo.createTollStation(tollStation);
		repo.removeTollStationById(tollStation.getId());
		assertEquals("TollStation removed", 0, repo.getTollStations().size());
	}

}
