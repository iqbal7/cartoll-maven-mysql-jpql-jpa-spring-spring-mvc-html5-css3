package se.par.amsen.cartoll.repository.jpa;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.repository.TollStationRepository;


public class TollStationRepositoryJpa extends AbstractRepositoryJpa implements TollStationRepository {

	@Transactional
	@Override
	public long createTollStation(TollStation tollStation) {
		em.persist(tollStation);
		return tollStation.getId();
	}

	@Transactional
	@Override
	public long updateTollStation(TollStation tollStation) {
		return em.merge(tollStation).getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TollStation> getTollStations() {
		return em.createNamedQuery("getAllTollStations").getResultList();
	}

	@Override
	public TollStation getTollStationById(long id) {
		return em.find(TollStation.class, id);
	}

	@Transactional
	@Override
	public boolean removeTollStationById(long id) {
		em.remove(em.find(TollStation.class, id));
		return em.find(TollStation.class, id) == null;
	}

	@Transactional
	@Override
	public void clearAllTollStations() {
		em.createNamedQuery("clearAllTollStations").executeUpdate();
	}
}
