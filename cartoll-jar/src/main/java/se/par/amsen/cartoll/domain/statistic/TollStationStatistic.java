package se.par.amsen.cartoll.domain.statistic;

/**
 * A TollStationStatistic inherits SimpleStatistic adding a stationId field to specify statistics for
 * a certain TollStation
 * @author Pär
 *
 */
public class TollStationStatistic extends SimpleStatistic{
	
	private long stationId;

	public TollStationStatistic(long stationId) {
		super();
		setStationId(stationId);
	}

	public TollStationStatistic(long stationId, long passages, long tax) {
		super(passages, tax);
		setStationId(stationId);
	}

	public long getStationId() {
		return stationId;
	}

	public void setStationId(long stationId) {
		this.stationId = stationId;
	}
	
}
