package se.par.amsen.cartoll.repository.mock;

public abstract class GenerateUniqueIdMock{
	
	private long id = 0l;
	
	public long generateUniqueId() {
		return ++id;
	}
}
