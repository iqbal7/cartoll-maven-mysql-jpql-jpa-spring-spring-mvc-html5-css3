package se.par.amsen.cartoll.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;

/**
 * Owner holds the data used to describe an Owner, first name, last name, SSID and
 * the Vehicles registered on the Owner in a List
 * @author Pär
 *
 */
@Entity
@NamedQueries(value={ 
		@NamedQuery(name="getAllOwners", query="SELECT o FROM Owner o ORDER BY lastName, firstName ASC"),
		@NamedQuery(name="clearAllOwners", query="DELETE FROM Owner")})
public class Owner {

	@Id
	@GeneratedValue
	private long id;
	
	private String firstName;
	private String lastName;
	private String SSID;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	private Adress adress;
	
	@OneToMany(mappedBy="owner", orphanRemoval=true, cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	private List<Vehicle> vehicles;

	public Owner(String firstName, String lastName, String SSID, Adress adress, List<Vehicle> vehicles) {
		this.firstName = firstName;
		this.setLastName(lastName);
		this.SSID = SSID;
		this.adress = adress;
		this.setVehicles(vehicles);
	}
	
	public Owner() { }

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String name) {
		this.firstName = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return firstName + " " + lastName;
	}

	public String getSSID() {
		return SSID;
	}

	public void setSSID(String sSID) {
		SSID = sSID;
	}

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
}
