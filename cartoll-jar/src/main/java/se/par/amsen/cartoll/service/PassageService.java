package se.par.amsen.cartoll.service;

import java.util.List;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.statistic.TollStationStatistic;

public interface PassageService {
	/**
	 * Pass Passage to repository and set its tax according to the tax calculation logic
	 * @return Passage Persisted passage with calculated tax
	 */
	public long createPassageAndCalculateTax(Passage passage);
	
	/**
	 * Update Passage and set its tax according to the tax calculation logic
	 * @return Passage Updated passage with calculated tax
	 */
	public long updatePassageAndCalculateTax(Passage passage);
	
	public List<Passage> getPassages();
	public Passage getPassageByPassageId(long id);
	public boolean removePassage(long id);
	
	public List<Passage> getPassagesForTollStation(long id);
	
	/**
	 * Construct a TollStationStatistic object with statistic data for a TollStation
	 * specified by its id.
	 * @param long id TollStation id
	 * @return TollStationStatistic for the specified TollStation.
	 */
	public TollStationStatistic getStatisticsByTollStationId(long id);
	
	/**
	 * Construct a SimpleStatistic object with summarized statistic data for all TollStations
	 * @return SimpleStatistic with summarized data for all TollStations
	 */
	public SimpleStatistic getStatisticsSummaryForAllTollStations();
	
	/**
	 * Construct a SimpleStatistic object with summarized statistic data for a Vehicle
	 * specified by its id.
	 * @return SimpleStatistic with summarized data for a Vehicle.
	 */
	public SimpleStatistic getStatisticsByVehicleId(long id);
	public void clearAllPassages();
}
