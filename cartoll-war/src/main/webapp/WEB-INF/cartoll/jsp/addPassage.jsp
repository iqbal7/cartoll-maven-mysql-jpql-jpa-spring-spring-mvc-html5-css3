<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="<c:url value="/resources/styles/reset.css" />" rel="stylesheet">
<link href="<c:url value="/resources/styles/main.css" />" rel="stylesheet">
<c:choose>
	<c:when test="${addNewPassage}">
		<c:set var="title" value="Add Passage"></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="title" value="Confirm Passage"></c:set>
	</c:otherwise>
</c:choose>

<title>${title}</title>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="content">
			<div id="stations">
				<div id="stationsList">
					<fieldset>
						<legend>${title}</legend>
						<c:choose>
							<c:when test="${addNewPassage}">
								<form:form commandName="passageBean">
									<label>Vehicle</label> 
									<form:select path="vehicleId" id="selector">
										<form:options items="${vehicleBeans}" itemValue="id" itemLabel="name"/>
									</form:select>
									<label>Toll station</label> 
									<form:select path="tollStationId" id="selector">
										<form:options items="${tollStationBeans}" itemValue="id" itemLabel="name"/>
									</form:select>
									<label>Time (h/m)</label>
									<div>
										<form:select path="hour" id="selectorHour">
											<c:forEach begin="0" end="23" var="hour">
												<form:option value="${hour}"/>
											</c:forEach>
										</form:select>
										<form:select path="minute" id="selectorMinute">
											<c:forEach begin="0" end="59" var="minute">
												<form:option value="${minute}"/>
											</c:forEach>
										</form:select>
									</div>
									<input type="submit" value="Submit" />
								</form:form>
							</c:when>
							<c:otherwise>
								<label>ID</label><p>${passage.id}</p>
								<label>Vehicle</label><p>${passage.vehicle.detailedType}</p>
								<label>Toll station</label><p>${passage.tollStation.name}</p>
								<label>Time</label><p>${passage.formattedTime}</p>
								<label>Tax</label><p>${passage.tax}</p>
								<a href="<c:url value="/addPassage.html"/>">Register a new passage</a>
							</c:otherwise>
						</c:choose>
					</fieldset>
				</div>
			</div>
			<div id="rightContent"></div>
		</div>
	</div>
</body>
</html>